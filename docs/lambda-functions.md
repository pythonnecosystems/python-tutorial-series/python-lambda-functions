# Python Lambda 함수: 익명 함수

## <a name="intro"></a> 개요
Python lambda 함수에 대한 이 포스팅에서는 다음과 같은 내용을 설명한다.

- lambda 함수는 무엇이며 Python의 일반 함수와 어떻게 다른가
- 단일 코드 행에서 lambda 함수를 정의하고 사용하는 방법
- lambda 함수 사용의 장점과 단점
- Python 프로그래밍과 데이터 분석의 다양한 시나리오와 문제에 lambda 함수를 적용하는 방법

이 글을 읽고 이해한다면 Python에서 lambda 함수를 생성하고 사용하여 간결하고 우아한 코드를 작성할 수 있다.

그러나 lambda 함수의 세부 사항으로 들어가기 전에 먼저 Python의 함수에 대한 기본 개념을 tkgvu 보자.

## <a name="sec_02"></a> Lambda 함수란?
lambda 함수는 Pthon에서 특수한 형태의 함수로서 코드 한 줄에 익명 함수를 만들 수 있다. 익명 함수는 이름이 없는 함수로서 `def` 키워드를 사용하지 않고 정의된다.

lambda 함수는 **lambda expression** 또는 **lambda form**으로도 알려져 있다. 이들은 **map**, **filter**, **sort** 같이 매개변수로서의 함수를 필요로 하는 다른 함수에 대한 인수로 종종 사용된다. lambda 함수는 변수에 할당되거나 다른 함수로부터 반환될 수도 있다.

lambda 함수의 구문은 다음과 같다.

```python
lambda parameters: expression
```

**`lambda`** 키워드는 lambda 함수를 만들고 있다는 것을 나타낸다. **`paramters`**는 lambda 함수가 받아들이는 인수 이름이다. 쉼표로 구분된 0개 이상의 모수를 가질 수 있다. **`expression`**은 값을 반환하는 lambda 함수의 본문이다. lambda 함수에는 하나의 식만 있을 수 있고, 그것은 어떤 문장이나 할당도 포함할 수 없다.

다음은 두 개의 매개 변수를 사용하여 합을 반환하는 lambda 함수의 예이다.

```python
lambda x, y: x + y
```

이 lambda 함수는 다음과 같은 정규 함수에 해당한다.

```python
def add(x, y):
    return x + y
```

그러나 lambda 함수는 더 간결하고 이름이 없다. lambda 함수를 직접 **`print`** 같은 다른 함수의 인수로 사용할 수 있다.

```python
print((lambda x, y: x + y)(3, 4)) # prints 7
```

또는 변수에 lambda 함수를 할당하고 나중에 사용할 수 있다.

```python
add = lambda x, y: x + y
print(add(3, 4)) # prints 7
```

다음 절에서는 lambda 함수를 정의하고 사용하는 방법을 좀 더 자세히 살펴볼 것이다.

## <a name="sec_03"></a> Lambda 함수를 정의하고 사용하는 방법
이 절에서는 Python에서 lambda 함수를 정의하고 사용하는 방법을 알아볼 것이다. 우리는 다음 주제들을 다룰 것이다:

- 매개변수 수가 다른 lambda 함수를 만드는 방법
- lambda 함수를 다른 함수에 대한 인수로 사용하는 방법
- lambda 함수를 다른 함수의 반환 값으로 사용하는 방법
- 변수에 lambda 함수를 할당하는 방법
- 조건식과 함께 lambda 함수를 사용하는 방법

첫 번째 주제인 매개변수 수가 다른 lambda 함수를 만드는 방법부터 살펴보자.

### 매개변수 수가 다른 lambda 함수를 만드는 방법
앞 절에서 보았듯이 lambda 함수의 구문은 다음과 같다.

```python
lambda parameters: expression
```

**`parameters`**는 lambda 함수가 수용하는 인수의 이름이다. 쉼표로 구분하여 0개 이상의 매개변수를 가질 수 있다. 예를 들어, 여기에 매개변수를 사용하지 않고 문자열 "Hello, World!"를 반환하는 lambda 함수가 있다.

```python
lambda: "Hello, World!"
```

다음은 하나의 매개변수를 사용하여 제곱을 반환하는 lambda 함수이다.

```python
lambda x: x**2
```

다음은 두 개의 매개 변수를 사용하여 제품을 반환하는 lambda 함수이다.

```python
lambda x, y: x * y
```

그리고 여기에 세 개의 매개변수를 사용하여 그 합을 반환하는 lambda 함수가 있다.

```python
lambda x, y, z: x + y + z
```

또한 정규 함수에서와 마찬가지로 매개변수에 기본값을 사용할 수 있다. 예를 들어, 두 개의 매개변수를 사용하지만 두 번째 매개변수에 기본값 1을 할당하는 lambda 함수가 있다.

```python
lambda x, y=1: x * y
```

이는 만약 하나의 인수만을 가지고 lambda 함수를 호출한다면, 그것은 두 번째 인수에 기본값인 1을 사용할 것이라는 것을 의미한다.

```python
f = lambda x, y=1: x * y
print(f(2)) # prints 2
print(f(2, 3)) # prints 6
```

그러나 lambda 함수에서 키워드 인수 뒤에는 위치 인수를 사용할 수 없다. 예를 들어 다음과 같은 것은 유효하지 않다.

```python
lambda x=1, y: x * y # SyntaxError
```

\*과 \*\* 연산자를 사용하여 가변 수의 인수를 lambda 함수에 전달할 수도 있다. 예를 들어, 임의의 수의 위치 인수를 사용하여 그 합을 반환하는 lambda 함수가 있다.

```python
lambda *args: sum(args)
```

그리고 여기에 임의의 수의 키워드 인수를 사용하고 해당 키와 값이 포함된 사전을 반환하는 lambda 함수가 있다.

```python
lambda **kwargs: kwargs
```

예를 들면,

```python
f = lambda *args: sum(args)
print(f(1, 2, 3)) # prints 6
print(f(1, 2, 3, 4, 5)) # prints 15
g = lambda **kwargs: kwargs
print(g(name="Alice", age=25)) # prints {'name': 'Alice', 'age': 25}
print(g(color="red", shape="circle")) # prints {'color': 'red', 'shape': 'circle'}
```

이제 매개변수 수가 다른 lambda 함수를 만드는 방법을 알았으므로 lambda 함수를 다른 함수의 인수로 사용하는 방법을 살펴보자.

### lambda 함수를 다른 함수의 인수로 사용하는 방법
Python에서 종종 다른 함수를 인수로 받아들이는 함수인 고차 함수의 인수로 lambda 함수를 사용할 수 있다. 이러한 함수의 일반적인 예로는 `map()` 함수가 있다. 이 함수는 (리스트 같은) 반복 가능한 각 항목에 주어진 함수를 적용하고 결과와 함께 새로운 반복 가능한 객체를 반환한다.

lambda 함수를 `map()`과 함께 사용하여 리스트의 각 숫자를 제곱하는 코드는 다음과 같다.

```python
numbers = [1, 2, 3, 4, 5]
squared_numbers = map(lambda x: x**2, numbers)
print(list(squared_numbers))  # Output: [1, 4, 9, 16, 25]
```

이 예에서는 `map()`을 사용하여 lambda 함수 `lambda x: x**2`를 `numbers` 리스트의 각 요소에 적용하고 `list()`를 사용하여 결과를 리스트로 변환한다.

lambda 함수는 `filter()`와 `reduce()` 같은 다른 고차 함수들과 함께 사용될 수 있으며, 이들은 각각 반복 가능한 요소들로부터 요소들을 필터링하고 일련의 요소들을 단일 값으로 감소시킨다.

다음은 `filter()`와 함께 lambda 함수를 사용하여 리스트에서 짝수를 필터링하는 예이다.

```python
numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
even_numbers = filter(lambda x: x % 2 == 0, numbers)
print(list(even_numbers))  # Output: [2, 4, 6, 8, 10]
```

리스트의 모든 요소의 합을 계산하기 위해 `reduced()`와 함께 lambda 함수를 사용하는 예는 다음과 같다.

```python
from functools import reduce

numbers = [1, 2, 3, 4, 5]
sum_of_numbers = reduce(lambda x, y: x + y, numbers)
print(sum_of_numbers)  # Output: 15
```

이 예에서는 lambda 함수 `lambda x, y: x + y`가 `reduce()`를 사용하여 `numbers` 리스트의 요소를 누적으로 적용하여 모든 요소의 합이 생성된다.

lambda 함수를 변수에 먼저 할당하지 않고, lambda 함수가 다른 함수에 인수로 직접 전달될 수도 있다. 이것은 종종 특정한 맥락에서만 사용되는 짧은 일회성 함수에 대해 수행된다.

### lambda 함수를 다른 함수의 반환 값으로 사용하는 방법
Python에서 함수들은 그들의 반환 값으로 lambda 함수를 반환할 수도 있다. 이는 특정 매개변수를 기반으로 다른 함수를 만드는 함수를 생성하는 데 유용할 수 있다.

다음은 lambda 함수를 반환하여 주어진 수의 n차 거듭제곱을 계산하는 함수의 예이다.

```python
def power_function(n):
    return lambda x: x**n

square = power_function(2)
cube = power_function(3)
print(square(2))  # Output: 4 (2^2)
print(cube(2))    # Output: 8 (2^3)
```

위의 예에서 `power_function`은 매개변수 `n`을 취하고 인수 `x`를 `n`의 거듭제곱으로 올리는 lambda 함수를 반환한다. 그런 다음 각각 `2`와 `3`인 `power_function`을 호출하여 두 개의 새로운 함수인 제곱과 세제곱을 만든다. 이 새로운 함수는 수의 제곱과 세제곱을 계산하는 데 사용될 수 있다.

### 변수에 lambda 함수를 할당하는 방법
앞의 예에서 보았듯이 lambda 함수도 일반 함수와 마찬가지로 변수에 할당할 수 있다. 이를 통해 코드의 뒷부분에 lambda 함수를 다시 사용할 수 있다.

다음은 변수에 lambda 함수를 할당한 다음 이를 사용하여 수의 제곱을 계산하는 예이다.

```python
square = lambda x: x**2
print(square(5))  # Output: 25
```

이 예에서 lambda 함수 `lambda x: x**2`를 변수 `square`에 할당하며, 이를 계산하기 위해 숫자로 호출할 수 있다.

### 조건식과 함께 lambda 함수를 사용하는 방법
Python의 lambda 함수는 조건식도 포함할 수 있어 특정 조건에 따라 다르게 동작하는 더 복잡한 함수를 만들 수 있다.

다음은 숫자가 양수이면 절대값을, 음수이면 제곱의 음수를 반환하는 lambda 함수의 예이다.

```python
abs_or_neg_squared = lambda x: x if x >= 0 else -x**2
print(abs_or_neg_squared(5))   # Output: 5
print(abs_or_neg_squared(-3))  # Output: -9
```

이 예에서 lambda 함수 `lambda x: if x >= 0 else -x**2`는 입력 `x`가 `0`보다 크거나 같은지 여부를 검사한다. 입력 `x`가 0 보다 크거나 같으면 `x`를 반환하고 그렇지 않으면 `-x**2`를 반환한다.

이것으로 Python에서 lambda 함수를 정의하고 사용하는 방법에 대한 논의를 마무리한다. lambda 함수는 다양한 맥락에서 사용할 수 있는 짧은 일회성 함수를 만드는 강력한 도구이다.

## <a name="sec_04"></a> Lambda 함수의 장단점
본 절에서는 Python에서 lambda 함수 사용의 장단점에 대해 논의할 것이다. 다음 주제들을 다룰 것이다.

- lambda 함수를 사용하는 이유
- lambda 함수의 한계는 무엇일까?
- lambda 함수를 사용할 경우와 회피해야 할 경우

첫 번째 주제를 시작하겠다. lambda 함수를 사용하는 이유를 알아 보자.

### lambda 함수를 사용하는 이유
lambda 함수는 여러 가지 이유로 유용하다. lambda 함수를 사용할 때 얻을 수 있는 몇 가지 주요 이점은 다음과 같다.

- lambda 함수는 간결하고 우아하다. 이는 `def` 키워드를 사용하거나 함수에 이름을 붙이지 않고 익명의 함수를 한 줄의 코드로 쓸 수 있도록 해준다. 특히 lambda 함수를 다른 함수들에 대한 인수로 사용할 때 코드는 더 읽기 쉽고 표현적으로 만들어 질 수 있다.
- lambda 함수는 동적이고 유연하다. 사전에 정의되지 않고 런타임에 생성되어 사용될 수 있다. 이것은 어떤 입력이나 조건을 기반으로 즉시 함수를 생성해야 할 때 유용할 수 있다. 예를 들어, lambda 함수를 사용하여 사용자의 선호도를 기반으로 리스트에 대한 커스텀 정렬 함수를 만들 수 있다.
- lambda 함수는 기능적이고 강력하다. 이들은 함수를 1급(first-class) 객체로 사용한다는 개념에 기초한 함수 프로그래밍(functional programming) 패러다임을 지원한다. 즉, 함수를 값으로 취급하거나 변수에 할당하거나 인수로 전달하거나 다른 함수에서 반환할 수 있다. 이를 통해 다른 함수를 매개변수로 사용하거나 다른 함수를 결과로 반환하는 함수인 고차 함수를 작성할 수 있다. 예를 들어, 함수를 원소의 수열에 적용하고 새로운 수열을 반환하는 고차 함수인 `map`, `filter`, `reduce` 등의 함수가 내장된 lambda 함수를 사용할 수 있다.

다음은 Python에서 lambda 함수를 사용하는 예이다.

```python
# Use a lambda function to square each element in a list
nums = [1, 2, 3, 4, 5]
squares = list(map(lambda x: x**2, nums)) # map applies the lambda function to each element in nums and returns a map object
print(squares) # prints [1, 4, 9, 16, 25]
# Use a lambda function to filter out the even elements in a list
nums = [1, 2, 3, 4, 5]
odds = list(filter(lambda x: x % 2 == 1, nums)) # filter applies the lambda function to each element in nums and returns only the elements that satisfy the condition
print(odds) # prints [1, 3, 5]
# Use a lambda function to reduce a list to a single value
from functools import reduce # reduce is a built-in function in Python 2, but not in Python 3, so you need to import it from the functools module
nums = [1, 2, 3, 4, 5]
product = reduce(lambda x, y: x * y, nums) # reduce applies the lambda function to the first two elements in nums, then to the result and the next element, and so on, until there is only one element left
print(product) # prints 120
# Use a lambda function to create a custom sorting function for a list of strings
names = ["Alice", "Bob", "Charlie", "David", "Eve"]
# Sort the names by the length of the string
names.sort(key=lambda x: len(x)) # key is a parameter that takes a function that returns a value to compare for sorting
print(names) # prints ['Bob', 'Eve', 'Alice', 'David', 'Charlie']
# Sort the names by the last letter of the string
names.sort(key=lambda x: x[-1]) # x[-1] returns the last character of x
print(names) # prints ['Bob', 'David', 'Eve', 'Alice', 'Charlie']
```

보다시피 lambda 함수는 코드를 더 간결하고 역동적이며 기능적이고 강력하게 만들 수 있다. 그러나 lambda 함수에도 한계와 단점들이 있다.

### lambda 함수의 한계는 무엇일까?
lambda 함수는 여러 가지 장점을 제공하지만, 한계도 있다. Python에서 lambda 함수를 사용할 때의 주dy 한계는 다음과 같다.

1. **기능 제한**: lambda 함수는 단일 표현식으로 제한된다. 이것은 복잡한 논리나 여러 문장을 지원할 수 없다는 것을 의미한다. 만약 두 개 이상의 표현식으로 함수를 박성해야 한다면 여러분은 대신 일반 함수로 정의하여 사용해야 한다.
1. **가독성 문제**: lambda 함수는 코드를 더 간결하게 만들 수 있지만, 특히 함수 프로그래밍 개념에 익숙하지 않은 초보자나 개발자에게는 코드의 가독성을 떨어뜨릴 수 있다. 함수에 대한 설명적인 이름이 없고 lambda 키워드를 사용하면 함수의 목적을 이해하기가 더 어려워질 수 있다.
1. **디버깅의 어려움**: lambda 함수는 일반 함수보다 디버깅하기가 더 어려울 수 있다. lambda 함수는 익명이기 때문에 오류 메시지나 스택 트레이스에 나타나는 이름이 없다. 이 때문에 코드의 문제를 파악하고 해결하는 것이 더 어려워질 수 있다.
1. **제한된 문서**: lambda 함수는 일반 함수를 문서화하는 데 사용되는 `docstring`을 지원하지 않는다. 특히 더 큰 코드베이스에서 lambda 함수에 대한 문서화와 사용 예를 제공하는 것이 더 어려울 수 있음을 의미한다.

###  lambda 함수를 사용할 경우와 회피해야 할 경우
lambda 함수의 장점과 한계를 고려할 때 코드에서 lambda 함수를 사용할 때와 피해야 할 때를 이해하는 것이 중요하다.

**Lambda 함수를 사용할 경우**:

- 인라인으로 정의할 수 있는 작고 간단한 함수가 필요할 때
- `map`, `filter`, `reduce` 같은 고차 함수의 경우와 같이 함수를 다른 함수에 인수로 전달하고자 할 때
- 일반 함수를 사용하는 것이 과도한 효과를 가져올 수 있는 소규모의 일회성 작업을 하고 있을 때

**Lambda  함수를 사용을 회피해야 하는 경우**:

- 여러 개의 표현이나 문장으로 복잡한 함수를 써야 할 때
- 팀 환경에서 가독성과 코드 유지보수성이 특히 매우 중요할 때
- 함수에 대한 포괄적인 설명서와 예시를 제공해야 할 때

요약하자면, lambda 함수는 Pyton에서 간결하고 함수적인 코드를 작성하는 데 강력한 도구가 될 수 있다. 그러나 코드의 가독성과 한계를 염두에 두고 신중하게 사용해야 한다. lambda 함수를 적절하게 사용하면 더 표현적이고 효율적인 코드를 작성할 수 있다.

## <a name="sec_05"></a> Python의 Lambda 함수 예
이 절에서는 Python에서 lambda 함수를 다양한 용도로 사용하는 예와 다음 주제들을 다룰 것이다.

- 리스트 컴프리헨션과 함께 lambda 함수를 사용하는 방법
- 사전과 함께 lambda 함수를 사용하는 방법
- 데코레이터와 함께 lambda 함수를 사용하는 방법
- 재귀 시 lambda 함수를 사용하는 방법

첫 번째 주제인 리스트 컴프리헨션과 함께 lambda 함수를 사용하는 방법부터 시작하겠다.

### 리스트 컴프리헨션과 함께 lambda 함수를 사용하는 방법
리스트 컴프리헨션은 Python에서 리스트를 생성하는 간결하고 우아한 방법이다. 리스트 컴프리헨션은 각 요소에 변환이나 필터를 적용하여 리스트, 튜플, 문자열 또는 range 같은 기존 반복 가능한 것에서 새로운 리스트를 만들 수 있다. 리스트 컴프리헨션의 구문은 다음과 같다.

```python
[expression for element in iterable if condition]
```
`expression`은 반복 가능한 객체의 각 요소에 적용하려는 변환이다. `element`는 반복 가능한 객체의 각 요소를 나타내는 변수의 이름이다. `iterable`은 반복하려는 객체이다. `condition`은 특정 기준을 만족하는 요소만을 선택하는 데 사용할 수 있는 선택적 필터이다. 예를 들어, 여기에 숫자 목록에서 새로운 제곱 값 리스트를 만드는 리스트 컴프리헨션이 있다.

```python
nums = [1, 2, 3, 4, 5]
squares = [x**2 for x in nums] # apply the expression x**2 to each element x in nums
print(squares) # prints [1, 4, 9, 16, 25]
```

다음은 숫자 목록에서 홀수의 새로운 리스트를 만드는 리스트 컴프리헨션이다.

```python
nums = [1, 2, 3, 4, 5]
odds = [x for x in nums if x % 2 == 1] # apply the condition x % 2 == 1 to each element x in nums
print(odds) # prints [1, 3, 5]
```

리스트 컴프리헨션에서 lambda 함수를 표현식이나 조건으로 사용하여 더 복잡한 변환이나 필터를 만들 수 있다. 예를 들어, 여기에 각 숫자를 이진법으로 변환하는 lambda 함수를 사용하여 숫자 리스트에서 문자열의 새 리스트를 만드는 리스트 컴프리헨션이 있다.

```python
nums = [1, 2, 3, 4, 5]
binaries = [(lambda x: bin(x)[2:])(x) for x in nums] # apply the lambda function (lambda x: bin(x)[2:])(x) to each element x in nums
print(binaries) # prints ['1', '10', '11', '100', '101']
```

다음은 lambda 함수를 사용하여 유효한 정수가 아닌 문자열을 필터링하여 문자열 리스트에서 새로운 숫자 리스트를 만드는 리스트 컴프리헨션이다.

```python
strings = ["1", "2", "3", "4", "5", "a", "b", "c"]
nums = [int(x) for x in strings if (lambda x: x.isdigit())(x)] # apply the lambda function (lambda x: x.isdigit())(x) to each element x in strings
print(nums) # prints [1, 2, 3, 4, 5]
```

보다시피 lambda 함수는 리스트 컴프리헨션과 함께 사용하여 보다 강력하고 유연한 리스트 작업을 수행할 수 있다. 그러나 너무 복잡하거나 모호한 lambda 함수는 코드의 가독성과 유지 보수성을 떨어뜨릴 수 있으므로 사용하지 않도록 주의해야 한다. 일반적으로 리스트 컴프리헨션이 간단하고 명확할 때만 lambda 함수를 사용해야 하며 코드의 가독성과 표현력이 향상될 때만 사용해야 한다.

### 사전과 함께 lambda 함수를 사용하는 방법
Python의 사전(dictionary)은 키와 값의 쌍이며, lambda 함수는 사전과 함께 다양하게 사용될 수 있다. 한 가지 일반적인 사용 사례는 사전에 있는 모든 값에 변환을 적용하는 것이다. 사전 컴프리헨션에 lambda 함수를 사용하여 기존 사전의 키의 값을 제곱하여 새로운 사전을 만드는 방법의 예는 다음과 같다.

```python
nums = {'a': 1, 'b': 2, 'c': 3, 'd': 4, 'e': 5}
squared_nums = {k: v**2 for k, vin nums.items()} # v**2 식을 각 값 vin nums에 적용합니다
print (squared_nums) # print {'a': 1, 'b': 4, 'c': 9, 'd': 16, 'e': 25}
```

이 예에서는 lambda 함수 `lambda v: v**2`를 사전의 각 값을 제곱하는 식으로 사용한다.

### 데코레이터와 함께 lambda 함수를 사용하는 방법
Python의 데코레이터는 함수나 메서드의 동작을 수정하거나 확장하는 강력한 방법이다. lambda 함수는 함수에 기능을 추가하는 데코레이터로 사용될 수 있다. 함수의 인수와 반환 값을 인쇄하기 위해 lambda 함수를 데코레이터로 사용하는 방법의 예를 다음과 같이 보인다.

```python
def debug(func):
    return lambda *args, **kwargs: (print(f"Calling {func.__name__} with args={args} kwargs={kwargs}"),
                                    func(*args, **kwargs),
                                    print(f"{func.__name__} returned {func(*args, **kwargs)}"))

@debug
def add(a, b):
    return a + b
add(1, 2)
```
이 예에서 `debug` 함수는 함수 `func`를 인수로 하고 원래 함수를 호출하기 전과 호출한 후에 함수 이름, 인수 및 반환 값을 출력하는 새로운 함수를 반환하는 데코레이터이다.

### 재귀 시 lambda 함수를 사용하는 방법
재귀는 함수가 같은 문제의 더 작은 경우를 해결하기 위해 스스로를 부르는 기술이다. lambda 함수는 단순하고 익명인 함수에 더 많이 사용되지만 재귀와 함께 사용될 수도 있다. 재귀와 함께 lambda 함수를 사용하여 수의 factorial을 계산하는 방법의 예는 다음과 같다.

```python
factorial = lambda n: 1 if n == 0 else n * factorial(n - 1)
print(factorial(5)) # prints 120
```
이 예에서 lambda 함수 `lambda n: 1 if n == 0 else n * factorial(n - 1)`은 재귀를 사용하여 숫자 `n`의 factorial을 계산한다.

## <a name="summary"></a> 요약
이 포스팅에서는 Python에서 lambda 함수를 사용하는 방법을 설명하였다.

- lambda 함수는 무엇이며 Python의 일반 함수와 차이점
- 단일 코드 행에서 lambda 함수를 정의하고 사용하는 방법
- lambda 함수를 사용할 경우의 장점과 단점
- Python 프로그래밍과 데이터 분석의 다양한 시나리오와 문제에 lambda 함수를 적용하는 방법

lambda 함수는 Python의 강력하고 우아한 특징으로 익명의 함수를 간결하고 역동적인 방식으로 만들 수 있다. 특히 다른 함수의 인수나 반환 값으로 사용할 때 더 표현적이고 기능적인 코드를 작성하는 데 도움을 줄 수 있다. 그러나 lambda 함수는 하나의 표현식으로 제한되고, 이름이나 문서가 없으며, 너무 복잡하거나 모호할 때 가독성과 유지 보수성이 떨어지는 등의 몇 가지 한계와 단점도 가지고 있다. 따라서 lambda 함수는 코드의 명확성과 단순성을 향상시킬 때만 현명하게 절제하여 사용해야 한다.
