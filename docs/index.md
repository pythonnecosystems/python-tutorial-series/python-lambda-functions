# Python Lambda 함수: 익명 함수 <sup>[1](#footnote_1)</sup>

<font size="3">Python에서 익명 함수를 한 줄로 생성할 때 람다 함수를 사용하는 방법에 대해 알아본다.</font>

## 목차

1. [개요](./lambda-functions.md#intro)
1. [Lambda 함수란?](./lambda-functions.md#sec_02)
1. [Lambda 함수를 정의하고 사용하는 방법](./lambda-functions.md#sec_03)
1. [Lambda 함수의 장단점](./lambda-functions.md#sec_04)
1. [Python의 Lambda 함수 예](./lambda-functions.md#sec_05)
1. [요약](./lambda-functions.md#summary)

<a name="footnote_1">1</a>: [Python Tutorial 31 — Python Lambda Functions: Anonymous Functions](https://levelup.gitconnected.com/python-tutorial-31-python-lambda-functions-anonymous-functions-e8fad65e4f1f)를 편역한 것입니다.
